﻿#Область СлужебныеПроцедуры

Процедура ВыполнитьСписаниеИСместитьГП(ДатаНачалаОбработки,
										ДатаОкончанияОбработки)
										Экспорт
Запрос = Новый Запрос;
Запрос.Текст = 
"ВЫБРАТЬ
|	Товарная.Регистратор КАК Регистратор,
|	Товарная.Период КАК Период
|ИЗ
|	Последовательность.Товарная КАК Товарная
|ГДЕ
|	Товарная.Период >=&ДатаНачалаОбработки
|	И Товарная.Период<=&ДатаОкончанияОбработки
|	И Товарная.Регистратор.Проведен
|
|УПОРЯДОЧИТЬ ПО
|	Период,
|	Регистратор";
Запрос.УстановитьПараметр("ДатаНачалаОбработки", ДатаНачалаОбработки);
Запрос.УстановитьПараметр("ДатаОкончанияОбработки", ДатаОкончанияОбработки);
Результат = Запрос.Выполнить();
ВыборкаДетальныеЗаписи = Результат.Выбрать();
СсылкаПоследнегоОбработанногоДокумента = Неопределено;
Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
	Если Документы.ПродажаТоваров.СписатьСебестоимостьТоваровДокумента(
		ВыборкаДетальныеЗаписи.Регистратор) 
		Тогда
		СсылкаПоследнегоОбработанногоДокумента =
		ВыборкаДетальныеЗаписи.Регистратор;
	Иначе
		Прервать;
	КонецЕсли
КонецЦикла;
Если ЗначениеЗаполнено(СсылкаПоследнегоОбработанногоДокумента) 
	Тогда
	ГраницаПоследнегоОбработанногоДокумента = 
	СсылкаПоследнегоОбработанногоДокумента.МоментВремени();
	Последовательности.Товарная.УстановитьГраницу(
	ГраницаПоследнегоОбработанногоДокумента);
КонецЕсли;
КонецПроцедуры

#КонецОбласти