﻿
#Область ОбработчикиСобытий

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	 Объект.СуммаДокумента = Объект.Товары.Итог("Сумма");
КонецПроцедуры

&НаКлиенте
Процедура ТоварыЦенаПриИзменении(Элемент)
	СтрокаТабличнойЧасти = Элементы.Товары.ТекущиеДанные;
РаботаСДокументамиКлиент.РассчитатьСумму(СтрокаТабличнойЧасти);
Объект.СуммаДокумента = Объект.Товары.Итог("Сумма");
КонецПроцедуры

&НаКлиенте
Процедура ТоварыКоличествоПриИзменении(Элемент)
	СтрокаТабличнойЧасти = Элементы.Товары.ТекущиеДанные;
РаботаСДокументамиКлиент.РассчитатьСумму(СтрокаТабличнойЧасти);
Объект.СуммаДокумента = Объект.Товары.Итог("Сумма");
КонецПроцедуры

&НаКлиенте
Процедура ТоварыНоменклатураПриИзменении(Элемент) 	
	СтрокаТабличнойЧасти = Элементы.Товары.ТекущиеДанные;
	СтрокаТабличнойЧасти.Цена = РаботаСоСправочникамиВызовСервера.РозничнаяЦена
	(Объект.Дата, СтрокаТабличнойЧасти.Номенклатура); 
	РаботаСДокументамиКлиент.РассчитатьСумму(СтрокаТабличнойЧасти);

КонецПроцедуры

#КонецОбласти
